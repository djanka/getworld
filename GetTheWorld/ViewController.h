//
//  ViewController.h
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CredentialsAlert.h"
#import "CredentialsController.h"

@interface ViewController : UIViewController<CredentialsAlertViewDelegate, CredentialsControllerDelegate>





@end

