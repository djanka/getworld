//
//  main.m
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
