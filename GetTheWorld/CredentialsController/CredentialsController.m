//
//  CredentialsController.m
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import "CredentialsController.h"
#import <UIKit/UIKit.h>

const char * _Nonnull kServerURLString = "http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds";


@implementation CredentialsController


- (void)dealloc
{
    _delegate = nil;
    
    [super dealloc];
}

- (void)processCredentialsWithEmail:(NSString *)email password:(NSString *)password
{
    NSString *deviceType = [NSString stringWithFormat:@"%@-%@%@",
                            [[UIDevice currentDevice] model],
                            [[UIDevice currentDevice] systemName],
                            [[UIDevice currentDevice] systemVersion]];
    
    NSString *deviceId = [[NSUUID UUID] UUIDString];
    
    NSString *params = [NSString stringWithFormat:@"login=%@", email];                          // Set email as login
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&password=%@", password]];     // Set password
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&deviceType=%@", deviceType]]; // Set device type
    params = [params stringByAppendingString:[NSString stringWithFormat:@"&deviceId=%@", deviceId]];     // Set device ID (UDID)
    
    [self authorize:params];
}

- (void)authorize:(NSString *)authorizationData
{
    // Setup request
    NSString *queryString = [NSString stringWithUTF8String:kServerURLString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:queryString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0f];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[authorizationData dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setup session
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    // Process task
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if ([(NSHTTPURLResponse *)response statusCode] == 200)
                                              {
                                                  NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                  NSLog(@"Response data: %@", stringData);
                                                  [stringData release];
                                                  
                                                  [_delegate credentialWasProcessedWithResult:data];
                                              }
                                              else
                                              {
                                                  if (error != nil)
                                                  {
                                                      [self showResponseErrorAlert];
                                                      NSLog(@"Error occured: \n%@", [error description]);
                                                  }
                                              }
                                              
                                          }];
    [postDataTask resume];

}

#pragma mark - User alerts

- (void)showResponseErrorAlert
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Error!"
                          message:@"Some error occured when you tried to fetch data. Please, try again."
                          delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                          otherButtonTitles:nil];
    
    [alert show];
    [alert release];
}


@end
