//
//  CredentialsController.h
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CredentialsControllerDelegate

- (void)credentialWasProcessedWithResult:(NSData *) receivedData;

@end


@interface CredentialsController : NSObject<NSURLSessionDelegate>

@property (nonatomic, assign) id<CredentialsControllerDelegate> delegate;

- (void)processCredentialsWithEmail:(NSString *)email password:(NSString *)password;

@end
