//
//  GameInfoViewController.m
//  GetTheWorld
//
//  Created by Nurzhan on 18/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import "GameInfoViewController.h"


const static CGFloat kFieldWidth    = 240.0f;
const static CGFloat kFieldHeight   = 30.0f;
const static CGFloat kFieldOffset   = 15.0f;


@implementation GameInfoViewController


CGFloat heightOffsetContainer;

- (id)initWithData:(NSDictionary *)gameData
{
    self = [self init];
    
    if (self)
    {
        [self addBackgroundLayer];
        
        CGFloat initialOffsetY = 5.0f;
        CGFloat initialOffsetX = 5.0f;
        
        heightOffsetContainer = initialOffsetY;
        
        for (NSString *key in gameData)
        {
            id data = [gameData valueForKey:key];
            [self setupData:data forKey:key widthOffset:initialOffsetX heightOffset:heightOffsetContainer];
            
            // Multiply 3 because of additional row with data value and a row for visual offset between data
            heightOffsetContainer += kFieldOffset * 3.0f;
        }
    }
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)addBackgroundLayer
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = [self.view bounds];
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0f] CGColor],
                       (id)[[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0f] CGColor],
                       (id)[[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0f] CGColor],
                       nil];
    
    [self.view.layer insertSublayer:gradient atIndex:0];
}


- (void)setupData:(id)data forKey:(NSString *)baseKey widthOffset:(CGFloat)widthOffset heightOffset:(CGFloat)heightOffset
{
    CGRect gameParameterRect = CGRectMake(widthOffset, heightOffset, kFieldWidth, kFieldHeight);
    UILabel *gameParameter = [[UILabel alloc] initWithFrame:gameParameterRect];
    NSString *labelText = [baseKey stringByAppendingString:@":\n"];
    gameParameter.text = labelText;
    gameParameter.textColor = [UIColor blackColor];
    
    if ([data isKindOfClass:[NSString class]])
    {
        CGRect gameParameterValueRect = CGRectMake(widthOffset, heightOffset + kFieldOffset, kFieldWidth, kFieldHeight);
        UILabel *gameParameterValue = [[UILabel alloc] initWithFrame: gameParameterValueRect];
        NSString *labelInfoText = data;
        gameParameterValue.text = labelInfoText;
        gameParameterValue.textColor = [UIColor blackColor];
        
        [self.view addSubview:gameParameterValue];
        [gameParameterValue release];
    }
    else if ([data isKindOfClass:[NSDictionary class]])
    {
        CGFloat additionalOffsetX = widthOffset * 3.0f;
        
        for (NSString *innerKey in data)
        {
            id innerData = [data valueForKey:innerKey];
            heightOffsetContainer += kFieldOffset * 2.0f;
            [self setupData:innerData forKey:innerKey widthOffset:additionalOffsetX heightOffset:heightOffsetContainer];
        }
    }
    
    [self.view addSubview:gameParameter];
    [gameParameter release];
}


@end
