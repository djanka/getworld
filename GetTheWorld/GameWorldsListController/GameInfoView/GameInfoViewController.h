//
//  GameInfoViewController.h
//  GetTheWorld
//
//  Created by Nurzhan on 18/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameInfoViewController : UIViewController

- (id)initWithData:(NSDictionary *)gameData;

@end
