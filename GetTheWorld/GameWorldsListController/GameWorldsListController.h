//
//  GameWorldsListController.h
//  GetTheWorld
//
//  Created by Nurzhan on 18/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GameWorldsListController : UITableViewController<UITableViewDataSource, UITableViewDelegate>

- (id)initWithData:(NSData *)data;

@end
