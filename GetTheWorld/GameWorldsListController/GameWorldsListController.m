//
//  GameWorldsListController.m
//  GetTheWorld
//
//  Created by Nurzhan on 18/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import "GameWorldsListController.h"
#import "GameInfoViewController.h"

const char *kWorldsDataKey  = "allAvailableWorlds";
static const char *kNameKey        = "name";

static CGSize const kThumbnailSize = {128, 45};


@interface GameWorldsListController()
{
    NSArray *_gameWorldsData;
}

@end


@implementation GameWorldsListController

- (id)initWithData:(NSData *)data
{
    self = [self init];
    if (self)
    {
        NSError *error;
        NSPropertyListFormat plistFormat;
        NSDictionary * dataDictionary = [NSPropertyListSerialization propertyListWithData:data
                                                                       options:NSPropertyListMutableContainersAndLeaves
                                                                        format:&plistFormat
                                                                         error:&error];
        // If data parsing succeded, save parse result
        if (error == nil)
        {
            _gameWorldsData = [dataDictionary objectForKey:[NSString stringWithUTF8String:kWorldsDataKey]];
            [_gameWorldsData retain];
        }
        else
        {
            // Create empty array otherwise
            _gameWorldsData = [[NSArray alloc] init];
        }
    }
    
    return self;
}


- (void)dealloc
{
    [_gameWorldsData release];
    
    [super dealloc];
}

- (void) viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBarHidden = NO;
}


#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_gameWorldsData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kThumbnailSize.height + 10.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    NSDictionary *tempDict = [_gameWorldsData objectAtIndex:indexPath.row];
    NSString *gameName = [tempDict objectForKey:[NSString stringWithUTF8String:kNameKey]];
    
    cell.textLabel.text = gameName;
    
    cell.imageView.layer.masksToBounds = YES;
    cell.imageView.layer.cornerRadius = 5.0f;
    
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    GameInfoViewController *gameInfoViewController = [[GameInfoViewController alloc] initWithData:[_gameWorldsData objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:gameInfoViewController animated:YES];
    [gameInfoViewController release];
}


@end
