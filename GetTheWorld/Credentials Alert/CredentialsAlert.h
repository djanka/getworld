//
//  CredentialsAlert.h
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@protocol CredentialsAlertViewDelegate

- (void)userDidEnterCredential:(NSString *)email :(NSString *)password;

@end



@interface CredentialsAlert : UIView<UITextFieldDelegate>

@property(nonatomic, assign) id<CredentialsAlertViewDelegate> delegate;

- (void)showOnParentView:(UIView *)parentView;
- (void)dismiss;

@end


