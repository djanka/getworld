//
//  CredentialsAlert.m
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//
#import "CredentialsAlert.h"
#import <Foundation/Foundation.h>


const static CGFloat kCornerRadius  = 7.0f;
const static CGFloat kShadowOffset  = 5.0f;

const static CGFloat kFieldWidth    = 240.0f;
const static CGFloat kFieldHeight   = 30.0f;
const static CGFloat kFieldOffset   = 30.0f;


@interface CredentialsAlert()
{
    UIView *_dialogContainer;
    
    UITextField *_emailField;
    UITextField *_passwordField;
    UIButton *_loginButton;
}

@end

@implementation CredentialsAlert


- (id)init
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];

    if (self)
    {
        [self createContainerView];
        
        CGRect frameRect = [self calculateFrameRectForWidth:kFieldWidth
                                                     height:kFieldHeight];
        
        [self createEmailField:CGRectOffset(frameRect, .0f, -kFieldOffset)
                        onView:_dialogContainer];
        [self createPasswordField:CGRectOffset(frameRect, .0f, kFieldOffset)
                           onView:_dialogContainer];
        [self createLoginButton:CGRectOffset(frameRect, .0f, kFieldOffset * 3.0f)
                         onView:_dialogContainer];
    }
    
    return self;
}

- (void)dealloc
{
    [_emailField    removeFromSuperview];
    [_emailField    release];
    
    [_passwordField removeFromSuperview];
    [_passwordField release];
    
    [_loginButton   removeFromSuperview];
    
    [_dialogContainer removeFromSuperview];
    [_dialogContainer release];
    
    _delegate = nil;
    
    [super dealloc];
}

- (void)showOnParentView:(UIView *)parentView
{
    if (parentView != nil)
    {
        [parentView addSubview:self];
    }
}

- (void)dismiss
{
    [self removeFromSuperview];
    
    [_emailField setText:nil];
    [_passwordField setText:nil];
}

#pragma mark - Private methods

- (void)createContainerView
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGPoint center = CGPointMake(screenSize.width / 2.0f, screenSize.height / 2.0f);

    _dialogContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 250)];
    _dialogContainer.center = center;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _dialogContainer.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0f] CGColor],
                       (id)[[UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0f] CGColor],
                       (id)[[UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0f] CGColor],
                       nil];
    
    CGFloat cornerRadius = kCornerRadius;
    gradient.cornerRadius = cornerRadius;
    [_dialogContainer.layer insertSublayer:gradient atIndex:0];
    
    _dialogContainer.layer.cornerRadius = cornerRadius;
    _dialogContainer.layer.borderColor = [[UIColor colorWithRed:198.0f/255.0f
                                                          green:198.0f/255.0f
                                                           blue:198.0f/255.0f
                                                          alpha:1.0f] CGColor];
    _dialogContainer.layer.borderWidth = 1.0f;
    _dialogContainer.layer.shadowRadius = cornerRadius + kShadowOffset;
    _dialogContainer.layer.shadowOpacity = 0.1f;
    _dialogContainer.layer.shadowOffset = CGSizeMake(0 - (cornerRadius + kShadowOffset) / 2.0f,
                                                     0 - (cornerRadius + kShadowOffset ) / 2.0f);
    _dialogContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    _dialogContainer.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:_dialogContainer.bounds
                                                                   cornerRadius:_dialogContainer.layer.cornerRadius].CGPath;

    [self addSubview:_dialogContainer];
}


- (CGRect)calculateFrameRectForWidth:(float)fieldWidth height:(float) fieldHeight
{
    CGSize containerSize = _dialogContainer.bounds.size;
    
    CGRect frameRect = CGRectMake((containerSize.width - fieldWidth) / 2.0f,
                                  (containerSize.height - fieldHeight) / 2.0f,
                                  fieldWidth,
                                  fieldHeight);
    
    return frameRect;
}

- (void)createEmailField:(CGRect)frameRect onView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectOffset(frameRect, .0f, -30.0f)];
    [label autorelease];
    [label setText:@"Enter email:"];
    [label setTextColor:[UIColor blackColor]];
    
    _emailField = [[UITextField alloc] initWithFrame:frameRect];
    _emailField.borderStyle = UITextBorderStyleRoundedRect;
    _emailField.font = [UIFont systemFontOfSize:15.0f];
    _emailField.placeholder = @"Email";
    _emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    _emailField.keyboardType = UIKeyboardTypeEmailAddress;
    _emailField.returnKeyType = UIReturnKeyDone;
    _emailField.enablesReturnKeyAutomatically = YES;
    _emailField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _emailField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _emailField.delegate = self;
    
    [view addSubview:_emailField];
    [view addSubview:label];
}

- (void)createPasswordField:(CGRect)frameRect onView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectOffset(frameRect, .0f, -30.0f)];
    [label autorelease];
    [label setText:@"Enter password:"];
    [label setTextColor:[UIColor blackColor]];
    
    _passwordField = [[UITextField alloc] initWithFrame:frameRect];
    _passwordField.borderStyle = UITextBorderStyleRoundedRect;
    _passwordField.font = [UIFont systemFontOfSize:15.0f];
    _passwordField.placeholder = @"Password";
    _passwordField.autocorrectionType = UITextAutocorrectionTypeNo;
    _passwordField.keyboardType = UIKeyboardTypeDefault;
    _passwordField.returnKeyType = UIReturnKeyDone;
    _passwordField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _passwordField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _passwordField.secureTextEntry = YES;
    _passwordField.delegate = self;
    
    [view addSubview:_passwordField];
    [view addSubview:label];
}

- (void)createLoginButton:(CGRect)frameRect onView:(UIView *)view
{
    _loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_loginButton setFrame:frameRect];
    [_loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [_loginButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20.0f]];
    [_loginButton setTitleColor:[UIColor colorWithRed:0.0f green:0.5f blue:1.0f alpha:1.0f] forState:UIControlStateNormal];
    [_loginButton setTitleColor:[UIColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:0.5f] forState:UIControlStateHighlighted];
    [_loginButton.layer setCornerRadius:kCornerRadius];
    [_loginButton addTarget:self action:@selector(userDidEnterCredentials) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:_loginButton];
}

#pragma mark - Login handler

- (void)userDidEnterCredentials
{
    if (_delegate != nil)
    {
        [_delegate userDidEnterCredential:[_emailField text] :[_passwordField text]];
    }
    
    [_emailField resignFirstResponder];
    [_passwordField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _emailField || textField == _passwordField)
    {
        [textField resignFirstResponder];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailField || textField == _passwordField)
    {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (BOOL)endEditing:(BOOL)force
{
    return YES;
}

@end