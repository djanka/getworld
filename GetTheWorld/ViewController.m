//
//  ViewController.m
//  GetTheWorld
//
//  Created by Nurzhan on 17/10/15.
//  Copyright © 2015 Nurzhan. All rights reserved.
//

#import "ViewController.h"
#import "GameWorldsListController.h"

@interface ViewController()
{
    CredentialsAlert *_credentialsAlert;
    CredentialsController *_credentialsController;
    
    UIView *_indicatorView;
    
    GameWorldsListController *_dataViewController;
}

@end

@implementation ViewController


- (void)dealloc
{
    [self hideActivityIndicator];
    
    [_credentialsAlert dismiss];
    [_credentialsAlert release];
    
    [_credentialsController release];
    
    [_dataViewController release];
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self createCredentialsAlert];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self showCredentialsAlert];
    
    self.navigationController.navigationBarHidden = (_dataViewController == nil);
    
    [super viewWillAppear:animated];
}


- (void)userDidEnterCredential:(NSString *)email :(NSString *)password
{
    /** Test login/pass
     * email = @"ios.test@xyrality.com";
     * password = @"password";
     */ 
    
    _credentialsController = [[CredentialsController alloc] init];
    _credentialsController.delegate = self;
    [_credentialsController processCredentialsWithEmail:email password:password];
    
    [self showActivityIndicator];
}

- (void)credentialWasProcessedWithResult:(NSData *)receivedData
{
    [self hideActivityIndicator];
    
    if (!receivedData)
    {
        [self showEmptyDataAlert];
        return;
    }

    [_credentialsAlert dismiss];
    
    [self createAvailableDataScreen:receivedData];
    [self showAvailableData];
    
    [_credentialsController release];
}

#pragma mark - Private methods

- (void)setupNavigationBar
{
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Last session"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(showAvailableData)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    [anotherButton release];
}

- (void)createAvailableDataScreen:(NSData *)data
{
    if (_dataViewController)
    {
        [_dataViewController release], _dataViewController = nil;
    }
    
    _dataViewController = [[GameWorldsListController alloc] initWithData:data];
}

- (void)showAvailableData
{
    [self.navigationController pushViewController:_dataViewController animated:YES];
}

- (void)createCredentialsAlert
{
    _credentialsAlert = [[CredentialsAlert alloc] init];
    [_credentialsAlert setDelegate:self];
}

- (void)showCredentialsAlert
{
    [_credentialsAlert showOnParentView:[self view]];
}

- (void)showEmptyDataAlert
{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Empty data!"
                          message:@"Sorry, no data was received due to your request."
                          delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                          otherButtonTitles:nil];
    
    [alert show];
    [alert release];
}

#pragma mark - Activity indicator

- (void)showActivityIndicator
{
    _indicatorView = [[UIView alloc] initWithFrame:self.view.superview.bounds];
    [_indicatorView setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.75f]];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = _indicatorView.center;
    [_indicatorView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    [activityIndicator release];
    
    [self.view addSubview:_indicatorView];
}

- (void)hideActivityIndicator
{
    [_indicatorView removeFromSuperview];
    [_indicatorView release];
}


@end
